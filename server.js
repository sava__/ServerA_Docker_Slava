var restify = require('restify');
var fetch = require('node-fetch');
var dataSession = {};
//var port = process.env.PORT || 443;
//Config
//var PrivareKeyABbtIV = new Buffer([0xcf, 0x5e, 0x46, 0x20, 0x45, 0x5c, 0xd7, 0x19, 0x0f, 0xcb, 0x53, 0xed, 0xe8, 0x74, 0xf1, 0xa8 ]); //16bit
var logscreator = require('./node_modules/common/sl/logsworks');
var createlogs = logscreator.createlogs;
var fs = require("fs");

var ourCryptoA = require('./lsi/ourCryptoServerA');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
// This line is from the Node.js HTTPS documentation.
var httpsServerOptions   = {
    key          : fs.readFileSync('./key/keySSL.pem'),
    certificate   : fs.readFileSync('./key/certSSL.pem'),
    insecure: true
};
var serverA = restify.createServer(httpsServerOptions);
//var serverA = restify.createServer();

serverA.use(restify.plugins.acceptParser(serverA.acceptable))
serverA.use(restify.plugins.jsonp())
serverA.use(restify.plugins.bodyParser({ mapParams: false }))

serverA.use(
  function crossOrigin (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With')
    return next()
  }
)
/*serverA.get({path: '/benchmarkingresults/all'}, function(req, res, next) {
  var result=[];
   fs.readdirSync("./benchmarking/").forEach((file,index,arr) => {
    if(arr.length > 0) {
     fs.readFile(`./benchmarking/${file}`, 'utf8', function readFileCallback(err, data){
       var info = {};

       if(data) info = JSON.parse(data);
       info["sessionid"]=file.split('.')[0];
       result.push(info);
       if(index === arr.length-1)
         res.send(result);
     });
    } else {
      res.send([]);
    }
   });
 });*/
serverA.post({path: '/sessions/:sessionid/DPPs'}, function (req, res, next) {

  var sessionid = req.params.sessionid;
  console.log(req.body);
  console.log(sessionid);
  var data = hashing(req.body.params.data);
  var serverB = '';
  var timeAfterPSBrequest;
  var timeBeforeServerB;
  var timeAfterServerB;
  /*fs.exists(`./benchmarking/${sessionid}.json`, (exists) => {
    if(!exists) fs.writeFile(`./benchmarking/${sessionid}.json`, '', 'utf8',function(){}); // write it back
  });*/

  var newdata = data.map((item)=>{
    return {dppHash:item.dppHash,DPP:item.DPP}
  });
  console.log(newdata);
  dataSession = {
    ClientID:req.body.params.ClientID,
    Referrer:req.body.params.Referrer,
    Host:req.body.params.Host
  };
  console.log("DatSession!!!")
  console.log(dataSession);
  var timeStart = new Date().getTime();
//  dataSession = JSON.parse(JSON.parse(req.params.sessionid));
  sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'UI action','Info','A','','true'));
  fetch(`https://g42d5jw22c.execute-api.eu-central-1.amazonaws.com/test/psb/encrypted/${sessionid.toString()}/`, //https://a40llf38y1.execute-api.eu-central-1.amazonaws.com/dev/psb/encrypted/
    {
      method: 'POST',
      body: JSON.stringify(dataSession),
      headers: { 'Content-Type': 'application/json' }
    })
    .then(function (res) {
      return res.json()
    })
    .then(function (json) {
      timeAfterPSBrequest = new Date().getTime();
      serverB = json.serverB;
      console.log(json);
      dataSession.OS = json.OS;

      sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Forward','Info','A','','true'));

      var prem = [];
      var datatosend = {
        ClientID: dataSession.ClientID,
        Referrer:dataSession.Referrer,
        Host:dataSession.Host,
        encodeB: []
      };
      var transf = [];
      if(Array.isArray(json.key)){
        prem = Preliminary(data, json);
        datatosend.encodeB = encodingValue(prem);
        console.log("Before sending....")
        console.log(datatosend.encodeB)

      }
      var DPPhashError = [];
      newdata.forEach(function (elem) {
         var dpperror = [];
          if(Array.isArray(json.key)){
             dpperror = json.key.filter(function(item){
              return elem.dppHash === item.dppHash});
          }
           if(dpperror.length === 0){
            DPPhashError.push(elem.DPP)
          }
        });
      if(DPPhashError.length!==0){
        DPPhashError.forEach((item)=>{
        //  console.log(item);
         var DPPanalitics = {
            SessionID:sessionid,
            ClientID: dataSession.ClientID,
            Referrer:dataSession.Referrer,
            Host:dataSession.Host,
            OS: dataSession.OS,
            DPP:item,
          };


    //        sendAnaliticsToPSB('analitics',DPPanalitics); //analitics add

        });
      }
      sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'UI action','Info','A','','true'));
      timeBeforeServerB = new Date().getTime();
      fetch(`https://172.17.0.1:49162/sessions/${sessionid.toString()}/DPPs`,
        {
          method: 'POST',
          body: JSON.stringify({data: datatosend}),
          headers: { 'Content-Type': 'application/json' }
        })
        .then(function (result) {
          return result.json()
        }).then(function (json) {
          timeAfterServerB = new Date().getTime();
          console.log("JSON...")
          console.log(json);
          sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Forward','Info','A','','true'));
//          res.send('Error EventID: ' + getId(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Error','A','','true')));
            console.log(json.msg);
            /*fs.readFile(`./benchmarking/${sessionid}.json`, 'utf8', function readFileCallback(err, data){
              var info = {};
              if(data) info = JSON.parse(data);
              info['Start request on Server A (Server A)'] = timeStart;
              info['Time after PSB request to Server B (Server A)'] = timeAfterPSBrequest;
              info['Time before Server B (Server A)'] = timeBeforeServerB;
              info['End request from Server B (Server A)'] = timeAfterServerB;

              fs.writeFile(`./benchmarking/${sessionid}.json`, JSON.stringify(info), 'utf8',function(){}); // write it back
            });*/


            res.send(json.msg);
        }).catch(function(err) {
            sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Error','A','','true'));
            res.send('Error EventID: ' + createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Error','A','','true').EventId);
            console.log(err);
        });
    }).catch(function(err) {
        sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Error','A','','true'));
        console.log(err);
    });
  return next()
})

serverA.listen(8080, function () {
  console.log('%s listening at %s', serverA.name, serverA.url)
})
function transformation (data) {
  return data.map(function (item) {
    var newitem = item
    newitem.value = item.value.split('').reverse().join('')
    delete newitem['dppHash']
    delete newitem['dppKey']
    return newitem
  })
}
function Preliminary (data, dictionary) {

  return data.map(function (item) {
    var newitem = item;
    var dppKeys = dictionary.key.filter(function (elem) {return elem.dppHash === item.dppHash});

    //console.log(dppKeys)
    if (dppKeys.length == 0) {
       newitem['dppKey'] =dictionary.defaultKey;
       return newitem
     }
    else {
      newitem['dppKey'] = dppKeys[0].dppKey;
      return newitem
    }
  })
}
function hashing (data) {
  return data.map(function (item, index) {
    var ddpItem = item
    //console.log(ddpItem)
    ddpItem['dppHash']=ourCryptoA.text2Hash(item['DPP'])//gethash(item, index)
    return ddpItem
  })
}

function gethash (item, index) {
  if (index === 0) return 121
  else if (index === 1) return 122
  else return 512
}

function encodingValue (data) {
  //var absolutePath = path.resolve(keyPubB);
  //var publicKeyB = fs.readFileSync(absolutePath, "utf8");
  return data.map(function (item) {
    var newitem = item
    //newitem['dppEncode']=ourCryptoA.encryptKeyValPrivatePublic(PrivareKeyABbtIV, newitem['dppKey'], newitem['value']);
    var res=ourCryptoA.encryptKeyVal(newitem['dppKey'], newitem['value']);
    newitem['dppEncode']=res['dppEncode'];
    newitem['dppDigest']=res['dppDigest'];
    delete newitem['value'];
    delete newitem['dppKey'];
    delete newitem['userId'];
    delete newitem['dppHash'];
        //console.log(newitem)
    return newitem
  })
}

function sendLogsToPSB(logDB){
  fetch( 'https://a40llf38y1.execute-api.eu-central-1.amazonaws.com/dev/psb/logs_add',
  {
    method: 'POST',
    body: JSON.stringify(logDB),
    headers: { 'Content-Type': 'application/json' }
  })
  .then(function (result) {
    return result.json()
  }).then(function (json) {
//    console.log(json);
    console.log("Done logs to server PSB request");
}).catch(function(err) {
      console.log(err);
  });
}
function sendAnaliticsToPSB(database,datafor){
  console.log("In send analitics...");
//  console.log(database);
//  console.log(datafor);
  fetch( 'https://search-analytics-n2vgptqs7rlfmfaxz6yuef5rg4.eu-central-1.es.amazonaws.com/analiticsprdb/doc',

  //fetch( `https://z73awiwh7d.execute-api.us-east-1.amazonaws.com/dev/psb/${database}`,
  {
    method: 'POST',
    body: JSON.stringify(datafor),
    headers: { 'Content-Type': 'application/json' }
  })
  .then(function (result) {
    return result.json()
  }).then(function (json) {
//    console.log(json);
    console.log(`Done analitics to ${database} on server PSB request`);
}).catch(function(err) {
      console.log(err);
  });
}
function niceEncodeObject (data)
{
       return encodeURIComponent(data).replace(/[ !'()*]/g,
               function(c) { return '%' + c.charCodeAt(0).toString(16); });
}
